# Usercounter / Visitorcounter

Een simpele plugin dat het aantal bezoekers op een website telt.

## Werking
Telkens als er een persoon de pagina bezoekt, wordt het aantal bezoekers uit de database opgehaalt.
Dit aantal wordt weergegeven op de pagina, en er wordt 1 extra bezoeker aan het aantal bezoekers toegevoegd.