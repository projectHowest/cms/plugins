<?php
/**
 * Plugin Name:       Usercounter
 * Description:       Counts the current users on the page
 * Version:           1.10.3
 * Requires at least: 5.2
 * Requires PHP:      7.0
 * Author:            Maarten Vansever, Anton Baeckelandt
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 */

 if(!defined('ABSPATH')){
    exit;
 }

require_once(plugin_dir_path(__FILE__) . '/includes/usercounter-class.php');


function uc_plugin_activate() {
 
    // Add database table
    global $wpdb;
    $charset_collate = $wpdb->get_charset_collate();
    $table_name = $wpdb->prefix . "user_counter"; 

    $sql = "CREATE TABLE " . $table_name ." (
        `key` varchar(55) NOT NULL,
        value varchar(55) DEFAULT '' NOT NULL,
        PRIMARY KEY  (`key`)
        ) " . $charset_collate;

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

    dbDelta( $sql );

    // Add key for user count
    $wpdb->insert( 
        $table_name, 
        array( 
            'key' => 'user_count', 
            'value' => '0'
        ), 
        array( 
            '%s', 
            '%s' 
        ) 
    );

    add_option( 'Activated_Plugin', 'Plugin-Slug' );
}
register_activation_hook( __FILE__, 'uc_plugin_activate' );


function load_plugin() {
  
      if ( is_admin() && get_option( 'Activated_Plugin' ) == 'Plugin-Slug' ) {
  
          delete_option( 'Activated_Plugin' );
      }
  }

  add_action( 'admin_init', 'load_plugin' );

function register_usercounter(){
    register_widget('User_Counter_Widget');
}

add_action('widgets_init', 'register_usercounter');

?>